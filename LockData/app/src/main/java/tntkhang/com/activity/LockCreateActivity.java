package tntkhang.com.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.locks.Lock;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/17/2016.
 */
public class LockCreateActivity extends Activity{
    private EditText edtPassword;
    private EditText edtRepeatPassword;
    private EditText edtEmail;
    private Button btnCreate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_create);
        startActivity(new Intent(LockCreateActivity.this, MainActivity.class));
//        String isContainPass = Utilities.getSharePreference(this, Constant.SharePreferenceConstant.KEY_PASSWORD);
//        if (isContainPass != null && !("").equals(isContainPass)) {
//            startActivity(new Intent(LockCreateActivity.this, LockValidateActivity.class));
//            this.finish();
//        }
//
//        edtPassword = (EditText) findViewById(R.id.edt_password);
//        edtRepeatPassword = (EditText) findViewById(R.id.edt_password_repeat);
//        edtEmail = (EditText) findViewById(R.id.edt_email);
//        btnCreate = (Button) findViewById(R.id.btn_create);
//        btnCreate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String password = edtPassword.getText().toString();
//                String repeatPassword = edtRepeatPassword.getText().toString();
//                String email = edtEmail.getText().toString();
//
//                boolean isSavePassword = Utilities.setPassword(LockCreateActivity.this, password, repeatPassword, email);
//                if (isSavePassword) {
//                    Toast.makeText(LockCreateActivity.this, "Success", Toast.LENGTH_LONG).show();
//                    startActivity(new Intent(LockCreateActivity.this, MainActivity.class));
//                    LockCreateActivity.this.finish();
//                }
//            }
//        });
    }
}