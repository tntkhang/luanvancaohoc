package tntkhang.com.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import tntkhang.com.aes.AESManager;
import tntkhang.com.fragment.ApplicationFragment;
import tntkhang.com.fragment.GalleryFragment;
import tntkhang.com.fragment.NavigationDrawerFragment;
import tntkhang.com.fragment.SettingFragment;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.container, switchFragment(1))
                .commit();

        Utilities.setSharePreference(this, Constant.SharePreferenceConstant.KEY_PASSWORD, "1111");
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, switchFragment(position + 1))
                .commit();
        restoreActionBar();
    }

    public Fragment switchFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 1:
                mTitle = getString(R.string.title_section_gallery);
                fragment = new GalleryFragment();
                break;
            case 2:
                mTitle = getString(R.string.title_section_application);
                fragment = new ApplicationFragment();
                break;
            case 3:
                mTitle = getString(R.string.title_section_setting);
                fragment = new SettingFragment();
                break;
        }
        return fragment;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    //this is in your parent activity of your fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

}
