package tntkhang.com.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by tntkhang on 1/29/2016.
 */
public class ChangePasswordActivity extends Activity {
    private EditText edtPassword;
    private EditText edtRepeatPassword;
    private EditText edtEmail;
    private Button btnChange;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtRepeatPassword = (EditText) findViewById(R.id.edt_password_repeat);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtEmail.setText(Utilities.getSharePreference(this, Constant.SharePreferenceConstant.KEY_EMAIL));
        btnChange = (Button) findViewById(R.id.btn_change);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassword = edtPassword.getText().toString();
                String repeatPassword = edtRepeatPassword.getText().toString();
                String email = edtEmail.getText().toString();

                boolean isSavePassword = Utilities.setPassword(ChangePasswordActivity.this, newPassword, repeatPassword, email);
                Toast.makeText(ChangePasswordActivity.this, (isSavePassword) ? "Change password success" : "Change password fail !", Toast.LENGTH_LONG).show();
            }
        });
    }
}
