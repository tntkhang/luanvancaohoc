package tntkhang.com.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/24/2016.
 */
public class LockValidateActivity extends Activity implements View.OnClickListener{
    private EditText edtPassword;
    private Button btnLogin;
    private Button btnForget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_validate);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnForget = (Button) findViewById(R.id.btn_forget);

        btnLogin.setOnClickListener(this);
        btnForget.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login :
                String password = edtPassword.getText().toString();
                if (("").equals(password)) {
                    Toast.makeText(LockValidateActivity.this, "Please input password !", Toast.LENGTH_SHORT).show();
                } else {
                    // Validate password
                    String savedPassword = Utilities.getSharePreference(LockValidateActivity.this, Constant.SharePreferenceConstant.KEY_PASSWORD);
                    Log.i("tntkhang", "savedPassword: " + savedPassword);
                    boolean passwordCorrect = false;
                    try {
                        passwordCorrect = Password.check(password, savedPassword);
                    } catch (Exception e) {}

                    if (passwordCorrect) {
                        startActivity(new Intent(LockValidateActivity.this, MainActivity.class));
                        LockValidateActivity.this.finish();
                    } else {
                        Toast.makeText(LockValidateActivity.this, "Wrong password !", Toast.LENGTH_SHORT).show();                             edtPassword.setText("");
                    }

                }
                break;
            case R.id.btn_forget:
                AlertDialog.Builder forgetDialog = new AlertDialog.Builder(this)
                        .setTitle("Recover Password !")
                        .setMessage("Please check mail you are registered to recover password !")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Sent mail with Deep  Link to open Change password Activity
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                forgetDialog.show();
                break;
        }
    }
}
