package tntkhang.com.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
//import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tntkhang.com.activity.R;
import tntkhang.com.model.Image;

/**
 * Created by Admin on 1/20/2016.
 */
public class GalleryAdapter extends ArrayAdapter {
    private List<Image> listImagesPath;
    private int resource;
    private LayoutInflater inflater;
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;


    public GalleryAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.listImagesPath = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Image getItem(int position) {
        return listImagesPath.get(position);
    }

    @Override
    public int getCount() {
        return listImagesPath.size();
    }

    public List<Image> getSelected() {
        List<Image> results = new ArrayList<>();
        for (Image image: listImagesPath) {
            if (image.isSelected())
                results.add(image);
        }
        return results;
    }
    static class ImageHolder {
        ImageView imageView;
        ImageView imgCheck;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ImageHolder imageHolder ;
        if (v == null) {
            v = inflater.inflate(resource, parent, false);
            imageHolder = new ImageHolder();
            imageHolder.imageView = (ImageView) v.findViewById(R.id.img_thumnail);
            imageHolder.imgCheck = (ImageView) v.findViewById(R.id.img_check);
            v.setTag(imageHolder);
        } else {
            imageHolder = (ImageHolder) v.getTag();
        }
        final Image image = getItem(position);
        if (image.isSelected()) {
            imageHolder.imgCheck.setVisibility(View.VISIBLE);
        } else {
            imageHolder.imgCheck.setVisibility(View.INVISIBLE);
        }

//        Picasso.with(getContext())
//                .load(new File(image.getPath()))
//                .resize(500, 500)
//                .centerCrop()
//                .placeholder(R.drawable.loading)
//                .into(imageHolder.imageView);
//        imageHolder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (image.isSelected()) {
//                    image.setIsSelected(false);
//                } else {
//                    image.setIsSelected(true);
//                }
//                notifyDataSetChanged();
//            }
//        });

        return v;
    }

}
