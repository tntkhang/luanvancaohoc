package tntkhang.com.model;

/**
 * Created by Admin on 1/20/2016.
 */
public class Image {
    private String path;
    private boolean isSelected;

    public Image(String path, boolean isSelected) {
        this.path = path;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
