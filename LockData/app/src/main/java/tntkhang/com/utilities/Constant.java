package tntkhang.com.utilities;

/**
 * Created by Admin on 1/19/2016.
 */
public class Constant {

    public static final boolean isEnable = false;
    public static final String GET_ALL_PATH = "get_all_path";
    public static class SharePreferenceConstant {
        public static final String SP_APP_NAME = "lock_data";
        public static final String KEY_PASSWORD = "key_password";
        public static final String KEY_EMAIL = "key_email";
    }
    public static class DeepLink {
        public static final String RECOVERY_APP = "http://www.tntkhang.lockdata/recoveryapp";
    }
}
