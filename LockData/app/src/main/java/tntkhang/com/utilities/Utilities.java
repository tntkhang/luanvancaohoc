package tntkhang.com.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;

/**
 * Created by Admin on 1/19/2016.
 */
public class Utilities {

    public static String getSharePreference(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static void setSharePreference(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean setPassword(Context context, String password, String repeatPassword, String email) {
        boolean isSuccess = false;
        if (!password.equals(repeatPassword)) {
            Toast.makeText(context, "Repeat password doesn't match with Password !", Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else if (("").equals(email)) {
            Toast.makeText(context, "Email Null !", Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else if (!email.contains("@")) {
            Toast.makeText(context, "Email is incorrect form !", Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else {
            // Pass validate password
            String saltedHash = "";
            try {
                saltedHash = Password.getSaltedHash(password);
            }catch(Exception e){

            }
            Utilities.setSharePreference(context, Constant.SharePreferenceConstant.KEY_PASSWORD, saltedHash);
            Utilities.setSharePreference(context, Constant.SharePreferenceConstant.KEY_EMAIL, email);
            Log.i("tntkhang", "Salt: " + saltedHash);
            isSuccess = true;
        }
        return isSuccess;
    }

    public static void sendEmail (Context context) {
        String email = Utilities.getSharePreference(context, Constant.SharePreferenceConstant.KEY_EMAIL);
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/html");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
        i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name) + "recovery password");
        String bodyEmail = context.getString(R.string.body_email) + "/n" + Constant.DeepLink.RECOVERY_APP;
        i.putExtra(Intent.EXTRA_TEXT , Html.fromHtml(bodyEmail));
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
