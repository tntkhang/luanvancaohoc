package tntkhang.com.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;


import tntkhang.com.activity.R;

/**
 * Created by Admin on 1/17/2016.
 */
public class SettingFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private static final int REQUEST_CODE_ENABLE = 11;
    private Switch switchEnable;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmNewPassword;
    private Button btnChangePass;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        switchEnable = (Switch) v.findViewById(R.id.switch_enable);
        edtOldPassword = (EditText) v.findViewById(R.id.edt_old_password);
        edtNewPassword = (EditText) v.findViewById(R.id.edt_new_password);
        edtConfirmNewPassword = (EditText) v.findViewById(R.id.edt_confirm_password);
        btnChangePass = (Button) v.findViewById(R.id.changePassword);

        switchEnable.setOnCheckedChangeListener(this);
        btnChangePass.setOnClickListener(this);

        return v;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
             switchEnable.setChecked(false);
            Toast.makeText(getActivity(), "Disable App !", Toast.LENGTH_LONG).show();
        } else {
            switchEnable.setChecked(true);
            Toast.makeText(getActivity(), "Enable App !", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {

    }
}
