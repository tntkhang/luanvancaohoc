package tntkhang.com.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tntkhang.com.activity.CustomGalleryActivity;
import tntkhang.com.adapter.GalleryAdapter;
import tntkhang.com.activity.R;
import tntkhang.com.aes.AESImageAdapter;
import tntkhang.com.aes.AESManager;
import tntkhang.com.model.Image;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/19/2016.
 */
public class GalleryFragment extends Fragment {

//    private static final String ACTION_PICK_IMAGE_MULTIPLE = "tntkhang.lockdata.ACTION_PICK_IMAGE_MULTIPLE";
    static final int ACTIVITY_SELECT_IMAGE = 0;
//    private ArrayList<Image> listImagesPath;
//    private GalleryAdapter galleryAdapter;
    private GridView gridView;
//    private static final int PICK_IMAGE_MULTIPLE = 1;
    private final int PASSWORD_LENGTH = 16;
    private static int IMAGE_SIZE = 500;
//    private String imageEncoded;
//    private List<String> imagesEncodedList;
    private AESImageAdapter mImageAdapter;
    private Handler mHandler;
    private byte[] mPasswordBytes = {0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0}; // the default password is sixteen 0 bytes

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery, null);
        gridView = (GridView) v.findViewById(R.id.gridViewImage);

//        listImagesPath = new ArrayList<>();
//        galleryAdapter = new GalleryAdapter(getActivity(), R.layout.fragment_gallery_row, listImagesPath);

//        gridView.setAdapter(galleryAdapter);

        mHandler = new Handler();
        mPasswordBytes = formatPassword(Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_PASSWORD));
        mImageAdapter = new AESImageAdapter(getActivity(), mPasswordBytes, mHandler,
                IMAGE_SIZE, IMAGE_SIZE);

        gridView.setOnItemClickListener(new ImagePreviewClickListener());
        gridView.setAdapter(mImageAdapter);

        setHasOptionsMenu(true);
        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
//        getImagesPath(getActivity(), listImagesPath);
//        galleryAdapter.notifyDataSetChanged();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
//                Intent intent = new Intent(ACTION_PICK_IMAGE_MULTIPLE);
//                Intent intent = new Intent(getActivity(), CustomGalleryActivity.class);
//                startActivityForResult(intent, 100);

                Intent i = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
                break;
            case R.id.action_unlock:
                decryptSelectedImage();
//                for(Image img: listImagesPath){
//                    if (img.isSelected()) {
//                        listImagesPath.remove(img);
//                    }
//                }
//                galleryAdapter.notifyDataSetChanged();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void decryptSelectedImage(){
        // Do all the decryption.
        // This will read from the encrypted file, and write a new decrypted file
        // in the same directory.
//        final String newFilePath = mFilename.substring((mFilename.lastIndexOf(AESManager.encryptedFolderString())-1) +
//                AESManager.encryptedFolderString().length(), mFilename.length());
//        final Handler uiHandler = new Handler();
//        AESManager.decrypt(mPasswordBytes, mFilename, newFilePath, ImageActivity.this,
//                new AESManager.EncryptionActionListener() {
//                    public void onActionComplete() {
//                        uiHandler.post(new Runnable() {
//                            public void run() {
//                                getActivity().finish();
//                            }
//                        });
//                    }
//                });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("tntkhang", "onActivityResult");
        try {
            // When an Image is picked
            if (resultCode == Activity.RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                final String filePath = cursor.getString(columnIndex);
                cursor.close();

                final String newFilePath = AESManager.encryptedFolderString() + filePath;


//                String[] allPath = data.getStringArrayExtra(Constant.GET_ALL_PATH);
                mPasswordBytes = formatPassword(Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_PASSWORD));
                AESManager.encrypt(mPasswordBytes, filePath, newFilePath, getActivity(), null);
//                mImageAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public byte[] formatPassword(String password) {
        byte[] passwordBytes = password.getBytes();
        byte[] newPassword = new byte[16];
        if (passwordBytes.length > PASSWORD_LENGTH) {
            for (int i = 0; i < PASSWORD_LENGTH; i++) {
                newPassword[i] = passwordBytes[i];
            }
        } else {
            for (int i = 0; i < passwordBytes.length; i++) {
                newPassword[i] = passwordBytes[i];
            }
            for (int i = passwordBytes.length; i < PASSWORD_LENGTH; i++) {
                newPassword[i] = 0;
            }
        }
        return newPassword;
    }

    class ImagePreviewClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            String filename = (String)mImageAdapter.getItem(position);

        }
    }
}
