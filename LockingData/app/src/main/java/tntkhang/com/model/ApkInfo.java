package tntkhang.com.model;

import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;

/**
 * Created by Khang Tran on 4/3/2016.
 */
public class ApkInfo {
    private String name;
    private String packageName;
    private String versionName;
    private int versionCode;
    private Drawable icon;
    private PackageInfo pInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public PackageInfo getpInfo() {
        return pInfo;
    }

    public void setpInfo(PackageInfo pInfo) {
        this.pInfo = pInfo;
    }
}
