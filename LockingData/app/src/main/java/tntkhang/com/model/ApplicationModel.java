package tntkhang.com.model;

import android.content.pm.PackageInfo;

/**
 * Created by Khang Tran on 4/1/2016.
 */
public class ApplicationModel implements Comparable<ApplicationModel> {
    private transient PackageInfo packageInfo;
    private String applicationName;
    private String packageName;
    private boolean isLocked;

    public ApplicationModel(PackageInfo packageInfo, String applicationName, String packageName, boolean isLocked) {
        this.packageInfo = packageInfo;
        this.applicationName = applicationName;
        this.packageName = packageName;
        this.isLocked = isLocked;
    }

    public PackageInfo getPackageInfo() {
        return packageInfo;
    }

    public void setPackageInfo(PackageInfo packageInfo) {
        this.packageInfo = packageInfo;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setIsLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public int compareTo(ApplicationModel other) {
        return applicationName.compareTo(other.getApplicationName());
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
