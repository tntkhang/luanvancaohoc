package tntkhang.com.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.locks.Lock;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/17/2016.
 */
public class LockCreateActivity extends Activity{
    private EditText edtPassword;
    private EditText edtRepeatPassword;
    private EditText edtEmail;
    private Button btnCreate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_create);
        String isContainPass = Utilities.getSharePreference(this, Constant.SharePreferenceConstant.KEY_PASSWORD);
        if (isContainPass != null && !("").equals(isContainPass)) {
            Intent intent = new Intent(LockCreateActivity.this, LockValidateActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
        } else {
            edtPassword = (EditText) findViewById(R.id.edt_password);
            edtRepeatPassword = (EditText) findViewById(R.id.edt_password_repeat);
            edtEmail = (EditText) findViewById(R.id.edt_email);
            btnCreate = (Button) findViewById(R.id.btn_create);
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String password = edtPassword.getText().toString();
                    String repeatPassword = edtRepeatPassword.getText().toString();
                    String email = edtEmail.getText().toString();
                    if (password.length() < Constant.PASSWORD_LENGTH) {
                        Toast.makeText(LockCreateActivity.this, getString(R.string.password_length_alert), Toast.LENGTH_LONG).show();
                    }
                    boolean isSavePassword = Utilities.setPassword(LockCreateActivity.this, password, repeatPassword, email);
                    if (isSavePassword) {
                        Toast.makeText(LockCreateActivity.this, getString(R.string.success), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LockCreateActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        LockCreateActivity.this.finish();
                    }
                }
            });
            startNotifyEnableAdminDialog();
        }
    }

    private void startNotifyEnableAdminDialog(){
        android.support.v7.app.AlertDialog.Builder enableSecurityMode = new android.support.v7.app.AlertDialog.Builder(this);
        enableSecurityMode.setTitle(getString(R.string.enable_administrator))
                .setMessage(getString(R.string.guide_turn_on_admin))
                .setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent settingIntent = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
                        settingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(settingIntent);
                    }
                })
                .setNegativeButton(getString(android.R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
