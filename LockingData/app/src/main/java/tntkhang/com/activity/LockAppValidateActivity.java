package tntkhang.com.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Khang Tran on 4/4/2016.
 */
public class LockAppValidateActivity extends Activity implements View.OnClickListener {
    private EditText edtPassword;
    private Button btnLogin;
    private Button btnForget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_validate);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnForget = (Button) findViewById(R.id.btn_forget);

        btnLogin.setOnClickListener(this);
        btnForget.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login :
                String password = edtPassword.getText().toString();
                if (("").equals(password)) {
                    Toast.makeText(LockAppValidateActivity.this, getString(R.string.please_input_password), Toast.LENGTH_SHORT).show();
                } else {
                    String savedPassword = Utilities.getSharePreference(LockAppValidateActivity.this, Constant.SharePreferenceConstant.KEY_PASSWORD);
                    boolean passwordCorrect = false;
                    try {
                        passwordCorrect = Password.check(password, savedPassword);
                    } catch (Exception e) {}

                    if (passwordCorrect) {
                        LockAppValidateActivity.this.finish();
                    } else {
                        Toast.makeText(LockAppValidateActivity.this, getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();                             edtPassword.setText("");
                    }

                }
                break;
            case R.id.btn_forget:
                Intent i = new Intent(this, ForgetPasswordActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                this.finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishScreen();
    }

    private void finishScreen(){
        Intent startHomescreen=new Intent(Intent.ACTION_MAIN);
        startHomescreen.addCategory(Intent.CATEGORY_HOME);
        startHomescreen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(startHomescreen);
    }
}
