package tntkhang.com.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.io.FileInputStream;

import tntkhang.com.aes.AESInputStream;
import tntkhang.com.aes.AESManager;
import tntkhang.com.view.ImageErrorView;

public class ImageActivity extends AppCompatActivity {
	
	public static final String BUNDLE_FILENAME = "filename";
	public static final String BUNDLE_PASSWORD = "password";
	
	String mFilename;
	byte[] mPasswordBytes;
	AsyncTask<String, Integer, Bitmap> mLoader;
	private ProgressBar progressBar;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar == null)
			throw new NullPointerException("Action bar is null");
        
        mFilename = getIntent().getExtras().getString(BUNDLE_FILENAME);
        mPasswordBytes = getIntent().getExtras().getByteArray(BUNDLE_PASSWORD);
        
		RelativeLayout layout = new RelativeLayout(this);
		progressBar = new ProgressBar(ImageActivity.this, null, android.R.attr.progressBarStyleLarge);
		progressBar.setIndeterminate(true);
		progressBar.setVisibility(View.VISIBLE);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		layout.addView(progressBar, params);

		setContentView(layout);

        
        mLoader = new LoadAESImage().executeOnExecutor(LoadAESImage.THREAD_POOL_EXECUTOR, mFilename);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_activity, menu);
        return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Decrypt the image
		if (item.getItemId() == R.id.remove) {
			decryptCurrentImage();
		}
		return false;
	}

	private void decryptCurrentImage() {        
        // Do all the decryption.
        // This will read from the encrypted file, and write a new decrypted file
        // in the same directory.
        final String newFilePath = mFilename.substring((mFilename.lastIndexOf(AESManager.encryptedFolderString())-1) +
        		AESManager.encryptedFolderString().length(), mFilename.length());
        final Handler uiHandler = new Handler();
		mLoader.cancel(true);
		AESManager.decrypt(mPasswordBytes, mFilename, newFilePath, ImageActivity.this,
				new AESManager.EncryptionActionListener() {
					public void onActionComplete() {
						uiHandler.post(new Runnable() {
							public void run() {
								ImageActivity.this.finish();
							}
						});
					}
				});

	}
    
    private class LoadAESImage extends AsyncTask<String, Integer, Bitmap> {
		@Override
		protected Bitmap doInBackground(String...imageFile) {
			Bitmap b;
			
			try {
				AESInputStream is = new AESInputStream(new FileInputStream(imageFile[0]), mPasswordBytes);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = false;
				
				b = BitmapFactory.decodeStream(is, null, options);

				is.close();
				
				return b;

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
				return null;
			}
			
		}
		
		protected void onPostExecute(Bitmap b) {
			progressBar.setVisibility(View.GONE);
			ImageView imageView = new ImageView(ImageActivity.this);
			imageView.setImageBitmap(b);
			ImageActivity.this.setContentView(imageView);			
		}
	}
}
