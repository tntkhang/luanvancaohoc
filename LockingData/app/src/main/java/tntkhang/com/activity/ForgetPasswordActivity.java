package tntkhang.com.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by tntkhang on 1/29/2016.
 */
public class ForgetPasswordActivity extends Activity implements View.OnClickListener{
    private EditText edtEmail;
    private EditText edtSecureCode;
    private Button btnSentSecureCode;
    private Button btnResetPassword;
    private String recoverEmail;
    private static final long FIVE_MINUTE = 300000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        edtSecureCode = (EditText) findViewById(R.id.edt_secure_code);
        edtEmail = (EditText) findViewById(R.id.edt_email);

        recoverEmail = Utilities.getSharePreference(this, Constant.SharePreferenceConstant.KEY_EMAIL);
        edtEmail.setText(recoverEmail);

        btnSentSecureCode = (Button) findViewById(R.id.btn_sent_secure_code);
        btnResetPassword = (Button) findViewById(R.id.btn_reset_password);

        btnSentSecureCode.setOnClickListener(this);
        btnResetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_sent_secure_code:
                String generateString = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(4, 10);
                Utilities.setSharePreference(ForgetPasswordActivity.this, Constant.SECURE_CODE, generateString);
                Utilities.setSharePreference(ForgetPasswordActivity.this, Constant.SENT_SECURE_CODE_TIME, System.currentTimeMillis() + "");
                sendMail(edtEmail.getText().toString(), "Locking App Reset Password", "Dear User,"
                        + "\n\n Your reset password secure code is: " + generateString
                        + "\n\n Thanks for using ours application."
                        + "Bests regards.");
                break;
            case R.id.btn_reset_password:
                long sentCodeTime = Long.parseLong(Utilities.getSharePreference(this, Constant.SENT_SECURE_CODE_TIME));
                long currentTime = System.currentTimeMillis();
                if (currentTime - sentCodeTime < FIVE_MINUTE) {
                    String inputSC = edtSecureCode.getText().toString();
                    String savedSC = Utilities.getSharePreference(this, Constant.SECURE_CODE);
                    if (savedSC.equals(inputSC)) {
                        Utilities.setSharePreference(this, Constant.SharePreferenceConstant.KEY_PASSWORD, "");
                        Intent intent = new Intent(this, LockCreateActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        this.finish();
                    } else {
                        Toast.makeText(this, getString(R.string.wrong_secure_code), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(this, getString(R.string.secure_code_expire), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("khanggameinfo@gmail.com", "infogamekhang");
            }
        });
    }
    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("lockingapp@support.com", "Khang Tran"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }
    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgetPasswordActivity.this, getString(R.string.please_wait), getString(R.string.sending_email), true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private void sendMail(String email, String subject, String messageBody) {
        Session session = createSessionObject();

        try {
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
