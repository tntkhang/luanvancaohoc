package tntkhang.com.services;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import tntkhang.com.activity.LockAppValidateActivity;
import tntkhang.com.activity.LockValidateActivity;
import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;
import tntkhang.com.model.ApkInfo;
import tntkhang.com.model.ApplicationModel;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Khang Tran on 4/3/2016.
 */
public class AppLauchCheckService extends Service {
    private int updateInterval;
    private ActivityManager am;
    private Handler handler;
    private Timer timer;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {

        super.onCreate();

        this.handler = new Handler();
        this.timer = new Timer();

        this.updateInterval = 1000;
        this.am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        this.checkForRunningApps();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private void checkForRunningApps() {

        this.timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {
                    private String lastFrontAppPkg = "";
                    private ApkInfo apkInfo;
                    private List<ApplicationModel> listLockedApp = Utilities.getJSONSharePrefs(AppLauchCheckService.this, Constant.SharePreferenceConstant.LIST_INSTALLED_APPLICATION);

                    @Override
                    public void run() {
                        if (listLockedApp != null && listLockedApp.size() != 0) {
                            List<ActivityManager.RunningAppProcessInfo> appProcesses = am
                                    .getRunningAppProcesses();
                            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                                appProcess = appProcesses.get(0);
                                try {
                                    if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                                        if (!lastFrontAppPkg.equals((String) appProcess.pkgList[0])) {
                                            apkInfo = getInfoFromPackageName(appProcess.pkgList[0]);
                                            if (apkInfo == null || apkInfo.getpInfo().applicationInfo.flags == ApplicationInfo.FLAG_SYSTEM) {
                                                // System App
                                                continue;
                                            }
                                            else if (((apkInfo.getpInfo().versionName == null)) || (apkInfo.getpInfo().requestedPermissions == null)) {
                                                // Application that comes preloaded with the device
                                                continue;
                                            }
                                            else {
                                                // Other App
                                                lastFrontAppPkg = (String) appProcess.pkgList[0];
                                            }
                                            if (!lastFrontAppPkg.equals(getApplicationContext().getPackageName())) {
                                                for (ApplicationModel item: listLockedApp) {
                                                    if (lastFrontAppPkg.equals(item.getPackageName())) {
                                                        String pkgAppLauched = Utilities.getSharePreference(AppLauchCheckService.this, Constant.APP_LOCK_PACKAGE_NAME);
                                                        if (!lastFrontAppPkg.equals(pkgAppLauched)) {
                                                            Intent lockIntent = new Intent(AppLauchCheckService.this, LockAppValidateActivity.class);
                                                            lockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(lockIntent);
                                                        }
                                                    }
                                                }
                                                Utilities.setSharePreference(AppLauchCheckService.this, Constant.APP_LOCK_PACKAGE_NAME, lastFrontAppPkg);
                                            }
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }
        }, 0, this.updateInterval);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.timer != null) {
            this.timer.cancel();
        }
    }

    private ApkInfo getInfoFromPackageName(String pkgName) {
        ApkInfo newInfo = new ApkInfo();
        try {
            PackageInfo p = this.getPackageManager().getPackageInfo(
                    pkgName, PackageManager.GET_PERMISSIONS);

            newInfo.setName(p.applicationInfo.loadLabel(
                    this.getPackageManager()).toString());
            newInfo.setPackageName(p.packageName);
            newInfo.setVersionName(p.versionName);
            newInfo.setVersionCode(p.versionCode);
            newInfo.setIcon(p.applicationInfo.loadIcon(this.getPackageManager()));
            newInfo.setpInfo(p);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return newInfo;
    }

    private int findPIDbyPackageName(String packagename) {
        int result = -1;

        if (this.am != null) {
            for (ActivityManager.RunningAppProcessInfo pi : this.am.getRunningAppProcesses()){
                if (pi.processName.equalsIgnoreCase(packagename)) {
                    result = pi.pid;
                }
                if (result != -1) break;
            }
        } else {
            result = -1;
        }

        return result;
    }

    private boolean isPackageRunning(String packagename) {
        return findPIDbyPackageName(packagename) != -1;
    }

}