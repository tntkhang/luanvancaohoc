package tntkhang.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import tntkhang.com.activity.R;
import tntkhang.com.fragment.ApplicationFragment;
import tntkhang.com.model.ApplicationModel;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Khang Tran on 4/1/2016.
 */
public class ApplicationFragmentAdapter extends RecyclerView.Adapter<ApplicationFragmentAdapter.ViewHolder> {
    private List<ApplicationModel> mDataset;
    private List<ApplicationModel> mDataLocked;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgAppIcon, icLockApp;
        public TextView tvAppName;
        public ViewHolder(View v) {
            super(v);
            imgAppIcon = (ImageView) v.findViewById(R.id.img_app_icon);
            tvAppName = (TextView) v.findViewById(R.id.tv_app_name);
            icLockApp = (ImageView) v.findViewById(R.id.ic_lock_app);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ApplicationFragmentAdapter(Context mContext, List<ApplicationModel> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        mDataLocked = Utilities.getSharePrefs(mContext, Constant.SharePreferenceConstant.LIST_INSTALLED_APPLICATION);;
        if (mDataLocked == null) {
            mDataLocked = new ArrayList<>();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ApplicationFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_application_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Drawable icon = mContext.getDrawable(android.R.drawable.ic_dialog_info); // Add android icon default

        try {
            icon = mContext.getPackageManager().getApplicationIcon(mDataset.get(position).getPackageInfo().packageName);
        }catch (Exception e){}

        holder.imgAppIcon.setImageDrawable(icon);

        holder.tvAppName.setText(mDataset.get(position).getApplicationName());


        if (mDataset.get(position).isLocked()) {
            holder.icLockApp.setImageDrawable(mContext.getDrawable(R.drawable.ic_lock_app));
        } else {
            holder.icLockApp.setImageDrawable(mContext.getDrawable(R.drawable.ic_unlock_app));
        }
        holder.icLockApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDataset.get(position).isLocked()) {
                    holder.icLockApp.setImageDrawable(mContext.getDrawable(R.drawable.ic_unlock_app));
                    mDataset.get(position).setIsLocked(false);
                    for(int i = 0; i < mDataLocked.size(); i++){
                        if(mDataLocked.get(i).getPackageName().equals(mDataset.get(position).getPackageName()))
                            mDataLocked.remove(i);
                    }
                } else {
                    holder.icLockApp.setImageDrawable(mContext.getDrawable(R.drawable.ic_lock_app));
                    mDataset.get(position).setIsLocked(true);
                    mDataLocked.add(mDataset.get(position));
                }
                Utilities.setSharePrefs(mContext, mDataLocked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


}
