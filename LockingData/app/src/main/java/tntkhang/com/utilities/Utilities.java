package tntkhang.com.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.List;

import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;
import tntkhang.com.model.ApplicationModel;

/**
 * Created by Admin on 1/19/2016.
 */
public class Utilities {

    public static String getSharePreference(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static void setSharePreference(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setJSONSharePrefs(Context context, String key, List<ApplicationModel> listApp) {
        SharedPreferences sharePrefs = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePrefs.edit();
        String connectionsJSONString = new Gson().toJson(listApp);
        editor.putString(key, connectionsJSONString);
        editor.commit();
    }



    public static void setSharePrefs(Context mContext, List<ApplicationModel> listApp) {
        SharedPreferences sharePrefs = mContext.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePrefs.edit();
        String connectionsJSONString = new Gson().toJson(listApp);
        editor.putString(Constant.SharePreferenceConstant.LIST_INSTALLED_APPLICATION, connectionsJSONString);
        editor.commit();
    }
    public static List<ApplicationModel> getSharePrefs(Context mContext, String key) {
        String connectionsJSONString = mContext.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE).getString(key, null);
        Type type = new TypeToken< List < ApplicationModel >>() {}.getType();
        List <ApplicationModel> listApp = new Gson().fromJson(connectionsJSONString, type);
        return listApp;
    }

    public static List<ApplicationModel> getJSONSharePrefs(Context context, String key) {
        String connectionsJSONString = context.getSharedPreferences(Constant.SharePreferenceConstant.SP_APP_NAME, Context.MODE_PRIVATE).getString(key, null);
        Type type = new TypeToken< List < ApplicationModel >>() {}.getType();
        List <ApplicationModel> listApp = new Gson().fromJson(connectionsJSONString, type);
        return listApp;
    }

    public static boolean setPassword(Context context, String password, String repeatPassword, String email) {
        boolean isSuccess = false;
        if (!password.equals(repeatPassword)) {
            Toast.makeText(context, context.getString(R.string.repeat_pw_doesnt_match), Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else if (("").equals(email)) {
            Toast.makeText(context, context.getString(R.string.email_null), Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else if (!EmailValidator.validate(email)) {
            Toast.makeText(context, context.getString(R.string.email_incorrect_form), Toast.LENGTH_LONG).show();
            isSuccess = false;
        } else {
            String saltedHash = "";
            try {
                saltedHash = Password.getSaltedHash(password);
            }catch(Exception e){

            }
            String passEncrytDescryt = getSharePreference(context, Constant.SharePreferenceConstant.KEY_ENCRYT_DESCRYT);
            if (("").equals(passEncrytDescryt)) {
                setSharePreference(context, Constant.SharePreferenceConstant.KEY_ENCRYT_DESCRYT, saltedHash);
            }

            setSharePreference(context, Constant.SharePreferenceConstant.KEY_PASSWORD, saltedHash);
            setSharePreference(context, Constant.SharePreferenceConstant.KEY_EMAIL, email);
            isSuccess = true;
        }
        return isSuccess;
    }
}
