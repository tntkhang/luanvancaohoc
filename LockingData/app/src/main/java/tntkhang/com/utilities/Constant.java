package tntkhang.com.utilities;

import android.os.Environment;

/**
 * Created by Admin on 1/19/2016.
 */
public class Constant {
    public static final String APP_LOCK_PACKAGE_NAME = "APP_LOCK_PACKAGE_NAME";
    public static final String SECURE_CODE = "SECURE_CODE";
    public static final String SENT_SECURE_CODE_TIME = "SENT_SECURE_CODE_TIME";
    public static final String ENCRYPT_FOLDER_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/LockingData/";
    public static final int PASSWORD_LENGTH = 6;
    public static class SharePreferenceConstant {
        public static final String SP_APP_NAME = "lock_data";
        public static final String KEY_PASSWORD = "key_password";
        public static final String KEY_ENCRYT_DESCRYT = "key_encryt_descryt";
        public static final String KEY_EMAIL = "key_email";
        public static final String LIST_INSTALLED_APPLICATION = "list_installed_application";
    }
}
