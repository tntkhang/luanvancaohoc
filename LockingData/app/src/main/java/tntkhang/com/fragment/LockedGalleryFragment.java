package tntkhang.com.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import tntkhang.com.activity.ImageActivity;
import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;
import tntkhang.com.aes.AESImageAdapter;
import tntkhang.com.aes.AESManager;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/19/2016.
 */
public class LockedGalleryFragment extends Fragment {
    static final int ACTIVITY_SELECT_IMAGE = 0;
    private GridView gridView;
    private final int PASSWORD_LENGTH = 16;
    private static int IMAGE_SIZE = 500;
    private AESImageAdapter mImageAdapter;
    private Handler mHandler;
    private byte[] mPasswordBytes = {0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0}; // the default password is sixteen 0 bytes

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery, null);
        gridView = (GridView) v.findViewById(R.id.gridViewImage);
        mHandler = new Handler();
        mPasswordBytes = formatPassword(Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_ENCRYT_DESCRYT));
        mImageAdapter = new AESImageAdapter(getActivity(), mPasswordBytes, mHandler,
                IMAGE_SIZE, IMAGE_SIZE);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String filename = (String) mImageAdapter.getItem(position);
                Intent imageIntent = new Intent(getActivity(), ImageActivity.class);
                imageIntent.setAction(Intent.ACTION_VIEW);
                imageIntent.putExtra(ImageActivity.BUNDLE_FILENAME, filename);
                imageIntent.putExtra(ImageActivity.BUNDLE_PASSWORD, mPasswordBytes);
                startActivity(imageIntent);
            }
        });
        gridView.setAdapter(mImageAdapter);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_section_gallery));
        setHasOptionsMenu(true);
        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:

                Intent i = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // When an Image is picked
            if (resultCode == Activity.RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                final String filePath = cursor.getString(columnIndex);
                cursor.close();

                final String newFilePath = AESManager.encryptedFolderString() + filePath;

                mPasswordBytes = formatPassword(Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_ENCRYT_DESCRYT));
                AESManager.encrypt(mPasswordBytes, filePath, newFilePath, getActivity(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public byte[] formatPassword(String password) {
        byte[] passwordBytes = password.getBytes();
        byte[] newPassword = new byte[16];
        if (passwordBytes.length > PASSWORD_LENGTH) {
            for (int i = 0; i < PASSWORD_LENGTH; i++) {
                newPassword[i] = passwordBytes[i];
            }
        } else {
            for (int i = 0; i < passwordBytes.length; i++) {
                newPassword[i] = passwordBytes[i];
            }
            for (int i = passwordBytes.length; i < PASSWORD_LENGTH; i++) {
                newPassword[i] = 0;
            }
        }
        return newPassword;
    }

}
