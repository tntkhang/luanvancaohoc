package tntkhang.com.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.commons.io.FileUtils;
import java.io.File;
import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Password;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 1/17/2016.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmNewPassword;
    private EditText edtEmail;
    private Button btnEditSettings;
    private Button btnClearData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        edtOldPassword = (EditText) v.findViewById(R.id.edt_old_password);
        edtNewPassword = (EditText) v.findViewById(R.id.edt_new_password);
        edtConfirmNewPassword = (EditText) v.findViewById(R.id.edt_confirm_password);
        edtEmail = (EditText) v.findViewById(R.id.edt_change_email);
        String currEmail = Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_EMAIL);
        edtEmail.setText(currEmail);

        btnEditSettings = (Button) v.findViewById(R.id.edit_settings);

        btnEditSettings.setOnClickListener(this);

        btnClearData = (Button) v.findViewById(R.id.btn_clear_app_data);
        btnClearData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage(getString(R.string.clear_data_alert));
                    alertDialog.setCancelable(true);

                    alertDialog.setPositiveButton(
                            getString(android.R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    try {
                                        Runtime runtime = Runtime.getRuntime();
                                        runtime.exec("pm clear " + getActivity().getApplicationContext().getPackageName());

                                        FileUtils.deleteDirectory(new File(Constant.ENCRYPT_FOLDER_PATH));
                                        dialog.cancel();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                    alertDialog.setNegativeButton(
                            getString(android.R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = alertDialog.create();
                    alert11.show();
            }
        });
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_section_setting));
        return v;
    }

    @Override
    public void onClick(View v) {
        String edtSetting = btnEditSettings.getText().toString();
        if (edtSetting.equals(getString(R.string.edit_settings))) {
            btnEditSettings.setText(getString(R.string.save_settings));
            enableEditText(true);
        } else if (edtSetting.equals(getString(R.string.save_settings))) {
            String oldPassword = edtOldPassword.getText().toString();
            String newPassword = edtNewPassword.getText().toString();
            String confirmNewPassword = edtConfirmNewPassword.getText().toString();
            String newEmail = edtEmail.getText().toString();

            if (newPassword.length() < Constant.PASSWORD_LENGTH) {
                Toast.makeText(getActivity(), getString(R.string.password_length_alert), Toast.LENGTH_LONG).show();
            }

            String savedPassword = Utilities.getSharePreference(getActivity(), Constant.SharePreferenceConstant.KEY_PASSWORD);
            boolean passwordCorrect = false;
            try {
                passwordCorrect = Password.check(oldPassword, savedPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (passwordCorrect) {
                Utilities.setPassword(getActivity(), newPassword, confirmNewPassword, newEmail);
                enableEditText(false);
                Toast.makeText(getActivity(), getString(R.string.success), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
            }
            edtOldPassword.setText("");
            edtNewPassword.setText("");
            edtConfirmNewPassword.setText("");
        }
    }

    private void enableEditText(boolean isEnable) {
        edtOldPassword.setEnabled(isEnable);
        edtNewPassword.setEnabled(isEnable);
        edtConfirmNewPassword.setEnabled(isEnable);
        edtEmail.setEnabled(isEnable);
    }
}
