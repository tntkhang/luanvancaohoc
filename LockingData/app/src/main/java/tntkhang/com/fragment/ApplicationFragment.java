package tntkhang.com.fragment;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import tntkhang.com.activity.MainActivity;
import tntkhang.com.activity.R;
import tntkhang.com.adapter.ApplicationFragmentAdapter;
import tntkhang.com.model.ApplicationModel;
import tntkhang.com.services.AppLauchCheckService;
import tntkhang.com.utilities.Constant;
import tntkhang.com.utilities.Utilities;

/**
 * Created by Admin on 2/24/2016.
 *
 * http://stackoverflow.com/questions/7248080/android-lock-apps
 */
public class ApplicationFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar pgLoadingApp;
    private List<ApplicationModel> mListPackageInfo = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_application, null);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        pgLoadingApp = (ProgressBar) v.findViewById(R.id.pg_loading_app);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        LoadPackageAsynTask loadPackageAsynTask = new LoadPackageAsynTask();
        loadPackageAsynTask.execute();

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ApplicationFragmentAdapter(getActivity(), mListPackageInfo);
        mRecyclerView.setAdapter(mAdapter);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_section_application));

        return v;
    }

    private List<ApplicationModel> getInstalledApps(Context ctx) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ApplicationModel> listApplicationInstalled = new ArrayList<>();
        final List<PackageInfo> allInstalledPackages = packageManager.getInstalledPackages(PackageManager.GET_META_DATA);


        final Set<ApplicationModel> filteredPackages = new HashSet();

        Drawable defaultActivityIcon = packageManager.getDefaultActivityIcon();

        for(PackageInfo each : allInstalledPackages) {
            if(ctx.getPackageName().equals(each.packageName)) {
                continue;  // skip own app
            }

            try {
                // add only apps with application icon
                Intent intentOfStartActivity = packageManager.getLaunchIntentForPackage(each.packageName);
                if(intentOfStartActivity == null)
                    continue;

                String appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(each.packageName, PackageManager.GET_META_DATA));

                Drawable applicationIcon = packageManager.getActivityIcon(intentOfStartActivity);
                if(applicationIcon != null && !defaultActivityIcon.equals(applicationIcon)) {
                    filteredPackages.add(new ApplicationModel(each, appName,each.packageName, false));
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.i("MyTag", "Unknown package name " + each.packageName);
            }
        }
        listApplicationInstalled.addAll(filteredPackages);
        Collections.sort(listApplicationInstalled);

        return listApplicationInstalled;
    }

    class LoadPackageAsynTask extends AsyncTask<Void, Void, List<ApplicationModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pgLoadingApp.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ApplicationModel> doInBackground(Void... params) {
            return getInstalledApps(getActivity());
        }

        @Override
        protected void onPostExecute(List<ApplicationModel> listPackgeInfo) {
            super.onPostExecute(listPackgeInfo);
            List<ApplicationModel> listLockedApp = Utilities.getSharePrefs(getActivity(), Constant.SharePreferenceConstant.LIST_INSTALLED_APPLICATION);
            if (listLockedApp != null) {
                for (ApplicationModel appInstalled: listPackgeInfo) {
                    for (ApplicationModel appLocked: listLockedApp) {
                        if (appInstalled.getPackageName().equalsIgnoreCase(appLocked.getPackageName())) {
                            appInstalled.setIsLocked(appLocked.isLocked());
                        }
                    }
                }
            }

            mListPackageInfo.addAll(listPackgeInfo);
            mAdapter.notifyDataSetChanged();
            pgLoadingApp.setVisibility(View.GONE);
        }
    }

}
